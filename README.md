# Ori Morningstar's Legal

[![Netlify Status](https://api.netlify.com/api/v1/badges/014575c8-d7fe-400f-b425-e74f3b8f86d4/deploy-status)](https://app.netlify.com/sites/legal-orimorningstar/deploys)

Micro front-end to display legal documentation for Ori Morningstar's applications

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/docs/deployment) for more information.
