import { defineNuxtConfig } from "nuxt";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      htmlAttrs: {
        lang: "en",
      },
      title: "Legal for Ori Morningstar",
      meta: [
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { name: "charset", content: "utf-8" },
      ],
      script: [
        {
          "data-domain": "legal.orimorningstar.com",
          src: "https://plausible.io/js/plausible.js",
          defer: true,
        },
      ],
    },
  },
});
