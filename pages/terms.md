# Terms and Conditions for Ori Morningstar

**Effective starting: 2022-04-16 10:56**

Welcome to Ori Morningstar!

These terms and conditions outline the rules and regulations for the use of [Ori Morningstar Services and Products](/services-and-products).

By accessing any of our services and products, we assume you accept these terms and conditions. Do not continue to use [Ori Morningstar Services and Products](/services-and-products) if you do not agree to take all the terms and conditions stated on this page.

The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.

## License

All software is licensed under the [GNU General Public License Version 3](/gnu-general-public-license-3) and later. Written works are licensed under [GNU Free Documentation License Version 1.3](gnu-free-documentation-license-1) and later.

This Agreement shall begin on the date hereof, except on occasions where the product or service was licensed beforehand. Our Terms and Conditions were created.

## Your Privacy

Please read [Privacy Policy for Ori Morningstar](/privacy)
