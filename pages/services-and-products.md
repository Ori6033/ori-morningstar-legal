# Ori Morningstar Services and Products

| Service or Product                                                                 | Date when License is effective |
| ---------------------------------------------------------------------------------- | ------------------------------ |
| [Ori Morningstar](https://orimorningstar.com/)                                     | April 16, 2022                 |
| [Ori Morningstar's Garden](https://garden.orimorningstar.com/Welcome+to+my+Garden) | April 16, 2022                 |
| [Binary Clock](https://binary-clock.org/)                                          | October 4, 2021                |
| [Poe](https://gitlab.com/Ori6033/poe)                                              | October 26, 2021               |
| [Pomodoros for Me](https://pomodorosforme.com/)                                    | April 16, 2022                 |
| [Legal](https://legal.orimorningstar/)                                             | May 16, 2022                   |
